EDI_AGM_20160302
TableName	Actflag	Created	Changed	AGMID	SecID	ISIN	IssID	AGMDate	AGMEGM	AGMNo	FYEDate	AGMTime	Add1	Add2	Add3	Add4	Add5	Add6	City	CntryCD	BondSecID
AGM	C	2015/12/16	2016/01/18	658105	2718922	CA033837AB17	169823	2016/01/18	BHM			10:00	Westwinds Conference Room,	2nd Floor Selkirk House,	555 4th Avenue S.W,	Calgary,	Alberta,			CA	2718922
AGM	C	2015/12/16	2016/01/18	658106	2276229	CA033837AA34	169823	2016/01/18	BHM			10:00	Westwinds Conference Room,	2nd Floor Selkirk House,	555 4th Avenue S.W.	Calgary,	Alberta,			CA	2276229
AGM	I	2016/01/18	2016/01/18	660321	1980667	IL0047301234	136187	2016/01/18	BHM			14:00	Co. Offices,	TLV						IL	1980667
AGM	I	2016/01/18	2016/01/18	660322	4138810	IL0047301499	21554	2016/01/18	BHM			14:00	Co. Offices,	TLV						IL	4138810
AGM	I	2016/01/18	2016/01/18	660330	715261	XS0307552355	106445	2016/02/05	BHM			:								NO	715261
AGM	I	2016/01/19	2016/01/19	660430	4288056	XS1078200273	82899	2015/10/07	BHM			:								AU	4288056
AGM	I	2016/01/19	2016/01/19	660512	3801663	XS0780192471	150131	2016/02/11	BHM			17:00	Offices of Milbank, Tweed,	Hadley & McCloy LLP,	At 3007 Alexandra House,	18 Chater Road,	Central, Hong Kong,			HK	3801663
AGM	I	2016/01/19	2016/01/19	660515	3925383	XS0852004299	150131	2016/02/11	BHM			17:00	At the offices of Milbank, Tweed, Hadley & McCloy	At 3007 Alexandra House,	18 Chater Road,	Central,	Hong Kong,			HK	3925383
AGM	I	2016/01/19	2016/01/19	660516	4241213	XS1054375446	150131	2016/02/11	BHM			17:00	At the offices of Milbank, Tweed, Hadley & McCloy	At 3007 Alexandra House,	18 Chater Road,	Central,	Hong Kong,			HK	4241213
AGM	U	2015/12/17	2016/01/20	658209	4178712	XS1013691024	178745	2016/01/04	BHM			10:00	K&L Gates, 44th Floor,Edinburgh Tower	The Landmark,15 Queen?s Road Central,	Hong Kong					KY	4178712
AGM	I	2016/01/20	2016/01/20	660531	299248	XS0201263372	95582	2016/01/27	BHM			:								NL	299248
AGM	I	2016/01/20	2016/01/20	660550	477706	XS0206052788	95435	2016/01/27	BHM			:								NL	477706
AGM	I	2016/01/20	2016/01/20	660552	297435	XS0179679328	95435	2016/01/27	BHM			:								NL	297435
AGM	I	2016/01/20	2016/01/20	660555	457493	XS0271448457	103446	2016/01/27	BHM			:								NL	457493
AGM	I	2016/01/20	2016/01/20	660558	299243	XS0201262309	95582	2016/01/27	BHM			:								NL	299243
AGM	C	2016/01/06	2016/01/20	659526	2564150	NO0010605025	91598	2016/01/20	BHM			13:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt 1,	0161 Oslo - 6th floor					NO	2564150
AGM	C	2016/01/06	2016/01/20	659527	2568213	NO0010605033	91598	2016/01/20	BHM			13:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt 1,	0161 Oslo - 6th floor					NO	2568213
AGM	C	2016/01/06	2016/01/20	659528	3893522	NO0010657174	91598	2016/01/20	BHM			13:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt 1,	0161 Oslo - 6th floor					NO	3893522
AGM	C	2016/01/06	2016/01/20	659529	2212742	NO0010590441	91598	2016/01/20	BHM			13:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt 1,	0161 Oslo - 6th floor					NO	2212742
AGM	I	2016/01/20	2016/01/20	660629	4039594	XS0934134312	156954	2016/02/17	BHM			10:00	White & Case LLP,	5 Old Broad Street,					London EC2N IDW	GB	4039594
AGM	U	2016/01/19	2016/01/21	660429	3838472	IL0011213340	20943	2016/01/24	BHM			17:45	Co. Offices,	TLV						IL	3838472
AGM	I	2016/01/21	2016/01/21	660649	753250	IL0049500361	21081	2016/01/27	BHM			16:00								IL	753250
AGM	I	2016/01/21	2016/01/21	660650	1980667	IL0047301234	136187	2016/01/20	BHM			:								IL	1980667
AGM	I	2016/01/21	2016/01/21	660652	4138810	IL0047301499	21554	2016/01/20	BHM			:								IL	4138810
AGM	I	2016/01/21	2016/01/21	660690	2659643	NO0010606320	113265	2016/02/03	BHM			13:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt 1,	0161 Oslo- 6th Floor					NO	2659643
AGM	C	2015/07/28	2016/01/22	639679	753376	IL0011045049	109708	2016/01/25	BHM			10:00	Co. Offices,	Herzliya						IL	753376
AGM	C	2015/07/28	2016/01/22	639680	1748728	IL0011174237	109708	2016/01/25	BHM			10:00	Co. Offices,	Herzliya						IL	1748728
AGM	U	2016/01/15	2016/01/22	660187	1385942	IL0011070930	128158	2016/01/21	BHM			16:00								IL	1385942
AGM	U	2015/06/13	2016/01/25	635144	139163	XS0162726987	80577	2015/07/01	BHM			:	At the offices Ashurst LLP,	Broadwalk House	5 Appold Street,	EC2A 2HA,	London			JE	139163
AGM	I	2016/01/25	2016/01/25	660926	1977553	US591555AA54	138141	2016/01/27	BHM			:								NL	1977553
AGM	I	2016/01/25	2016/01/25	660927	4372520	US591555AC11	138141	2016/01/27	BHM			:								NL	4372520
AGM	I	2016/01/25	2016/01/25	660928	2449363	XS0594146234	138141	2016/01/27	BHM			:								NL	2449363
AGM	I	2016/01/25	2016/01/25	660929	2433797	US591555AB38	138141	2016/01/27	BHM			:								NL	2433797
AGM	U	2015/11/30	2016/01/26	656460	484975	XS0205566259	106058	2015/12/16	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	London,	UNITED KINGDOM,				JP	484975
AGM	U	2015/12/01	2016/01/26	656567	479152	XS0205566846	106058	2015/12/16	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	London,	UNITED KINGDOM,				JP	479152
AGM	U	2015/12/01	2016/01/26	656568	478724	XS0205567224	106058	2015/12/16	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	London,	UNITED KINGDOM,				JP	478724
AGM	U	2015/12/01	2016/01/26	656569	478676	XS0205567653	106058	2015/12/16	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	London, E1 6AD	United kingdom,				JP	478676
AGM	U	2015/12/02	2016/01/26	656745	1445205	XS0199335984	123605	2015/12/18	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	E1 6AD,	London,	UNITED KINGDOM			KY	1445205
AGM	U	2015/12/02	2016/01/26	656746	1445202	XS0199335554	123605	2015/12/18	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	E1 6AD,	London,	UNITED KINGDOM			KY	1445202
AGM	U	2015/12/02	2016/01/26	656747	1452172	XS0199336446	123605	2015/12/18	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	London,	UNITED KINGDOM,				KY	1452172
AGM	U	2016/01/04	2016/01/26	659336	3385549	XS0289440637	121321	2016/01/15	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	E1 6AD,	London,	UNITED KINGDOM			KY	3385549
AGM	I	2016/01/26	2016/01/26	660948	479572	XS0205650988	106058	2015/12/16	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	London,	UNITED KINGDOM,				JP	479572
AGM	U	2016/01/25	2016/01/27	660890	1748728	IL0011174237	109708	2016/03/10	BHM			12:00	Co. Offices,	Herzliya						IL	1748728
AGM	U	2016/01/25	2016/01/27	660891	753376	IL0011045049	109708	2016/03/10	BHM			12:00	Co. Offices,	Herzliya						IL	753376
AGM	I	2016/01/29	2016/01/29	661341	4581322	IL0029701922	21078	2016/02/09	BHM			15:00	Co. Offices,	TLV						IL	4581322
AGM	I	2016/01/29	2016/01/29	661349	418844	CH0027073219	65463	2016/03/01	BHM			10:00	Allen & Overy LLP	One Bishops Square					London E1 6AD	GB	418844
AGM	I	2016/01/29	2016/01/29	661350	619546	XS0307322437	65463	2016/03/01	BHM			10:30	Allen & Overy LLP	One Bishops Square					London E1 6AD	GB	619546
AGM	I	2016/01/29	2016/01/29	661468	4441594	SG6VF1000009	157377	2016/03/03	BHM			10:00	TKP Conference Centre Raffles Place,	55 Market Street #03-01	Singapore 048941,	Conference Room 3				SG	4441594
AGM	I	2016/01/29	2016/01/29	661469	4494794	SG6WG5000001	157377	2016/03/03	BHM			10:30	TKP Conference Centre Raffles Place,	55 Market Street #03-01	Singapore 048941,	Conference Room 3				SG	4494794
AGM	U	2015/12/08	2016/02/01	657237	3766806	XS0285503248	144915	2016/02/15	BHM			11:30	offices of Elvinger,	Hoss & Prussen, 5,	Place Winston Churchill,				L-1340 Luxembourg	LU	3766806
AGM	U	2015/12/08	2016/02/01	657241	2919704	XS0252707624	144915	2016/02/15	BHM			11:00	offices of Elvinger,	Hoss & Prussen,5,	Place Winston Churchill				L-1340 Luxembourg	LU	2919704
AGM	I	2016/02/02	2016/02/02	661649	3838472	IL0011213340	20943	2016/02/03	BHM			15:00	Co. Offices,	TLV						IL	3838472
AGM	I	2016/02/02	2016/02/02	661658	4166337	XS1009480796	160393	2016/02/19	BHM			16:30	Premises of the Issuer,	Via Cadriano 27/2,					40127 ? Bologna	IT	4166337
AGM	U	2016/01/23	2016/02/02	660873	4394098	XS1145219652	138141	2016/01/27	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	London, E1 6AD	United kingdom,				NL	4394098
AGM	U	2016/01/20	2016/02/03	660532	299250	XS0201263612	95582	2016/01/27	BHM			:	Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	299250
AGM	U	2016/01/20	2016/02/03	660597	2427618	XS0591549232	138141	2016/01/27	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	London, E1 6AD	UNITED KINGDOM,				NL	2427618
AGM	U	2016/01/23	2016/02/03	660874	1924491	XS0511379066	138141	2016/01/27	BHM			:	The Offices Of Allen Overy LLP,	One Bishops Square,	E1 6AD,	London,	UNITED KINGDOM,			NL	1924491
AGM	U	2016/02/01	2016/02/03	661487	1980667	IL0047301234	136187	2016/02/02	BHM			10:00	Leonardo City Tower Hotel	Ramat-Gan						IL	1980667
AGM	U	2016/02/01	2016/02/03	661488	4138810	IL0047301499	21554	2016/02/02	BHM			10:00	Leonardo City Tower Hotel	Ramat-Gan						IL	4138810
AGM	I	2016/02/03	2016/02/03	661854	297441	XS0181842146	95435	2016/01/27	BHM			:	At the offices Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	297441
AGM	U	2016/01/20	2016/02/03	660535	457478	XS0271446592	103446	2016/01/27	BHM			:	At Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	457478
AGM	U	2016/01/20	2016/02/03	660529	569129	XS0287351463	108861	2016/01/27	BHM			:	At Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	569129
AGM	U	2016/01/20	2016/02/03	660530	299246	XS0201262721	95582	2016/01/27	BHM			:	At Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	299246
AGM	U	2016/01/20	2016/02/03	660537	569132	XS0287352198	108861	2016/01/27	BHM			:	At Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	569132
AGM	U	2016/01/20	2016/02/03	660546	297439	XS0179679914	95435	2016/02/18	BHM			:	At Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	297439
AGM	U	2016/01/20	2016/02/03	660554	428281	XS0271447210	103446	2016/02/18	BHM			:	At Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	428281
AGM	I	2016/02/03	2016/02/03	661871	569127	XS0287349566	108861	2016/02/18	BHM			:								NL	569127
AGM	U	2016/01/20	2016/02/04	660533	297437	XS0179679674	95435	2016/01/27	BHM			:	At the offices Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	297437
AGM	U	2016/01/20	2016/02/04	660534	569127	XS0287349566	108861	2016/01/27	BHM			:	At the offices Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	569127
AGM	U	2016/01/20	2016/02/04	660536	569126	XS0291271319	108861	2016/01/27	BHM			:	At the offices Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	569126
AGM	I	2016/02/04	2016/02/04	661991	2457061	IL0011213423	104908	2016/02/10	BHM			18:00								IL	2457061
AGM	I	2016/02/04	2016/02/04	661992	2072894	IL0011202863	104908	2016/02/10	BHM			18:00								IL	2072894
AGM	I	2016/02/04	2016/02/04	661994	428296	XS0271450784	103446	2016/01/27	BHM			:	At the offices Stibbe N.V.	Strawinskylaan 2001 1077,	ZZ Amsterdam					NL	428296
AGM	I	2016/02/04	2016/02/04	662019	2589430	CA77472PAA99	120488	2016/03/07	BHM			10:00	The offices of National Bank Financial Inc,	311 - 6th Avenue SW,	18th Floor,	Calgary,	Alberta T2P 3H2			CA	2589430
AGM	I	2016/02/04	2016/02/04	662152	4143565	NO0010694599	159590	2016/02/02	BHM			11:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt.1,	0161 Oslo- 5th Floor					NO	4143565
AGM	I	2016/02/05	2016/02/05	662188	4465314	IL0011133985	170688	2016/02/09	BHM			12:00								IL	4465314
AGM	I	2016/02/05	2016/02/05	662190	755112	IL0054901231	21410	2016/02/08	BHM			16:00								IL	755112
AGM	I	2016/02/05	2016/02/05	662191	4475887	IL0054901801	21410	2016/02/04	BHM			16:00								IL	4475887
AGM	I	2016/02/06	2016/02/08	662355	4411750		154953	2016/02/19	BHM			:								CN	4411750
AGM	I	2016/02/06	2016/02/08	662393	4022358	NO0010675572	155919	2016/02/22	BHM			13:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt,	0161 Oslo- 6th Floor					NO	4022358
AGM	I	2016/02/08	2016/02/08	662403	618662	XS0308397149	4510	2016/02/11	BHM			:								GB	618662
AGM	U	2016/02/09	2016/02/10	662482	4301305	IL0077101553	21009	2016/02/10	BHM			14:30	Co. Offices	TLV						IL	4301305
AGM	U	2016/02/09	2016/02/10	662483	4301310	IL0077101637	21009	2016/02/10	BHM			14:30	Co. Offices	TLV						IL	4301310
AGM	I	2016/02/10	2016/02/10	662588	4138810	IL0047301499	21554	2016/02/10	BHM			11:00	Co. Offices	Ramat-Gan						IL	4138810
AGM	I	2016/02/10	2016/02/10	662589	1980667	IL0047301234	136187	2016/02/10	BHM			11:00	Co. Offices	Ramat-Gan						IL	1980667
AGM	I	2016/02/11	2016/02/11	662731	3581884	XS0751016865	79961	2016/03/01	BHM			:								ZA	3581884
AGM	I	2016/02/11	2016/02/11	662732	2764909	XS0638008051	79961	2016/03/01	BHM			:								ZA	2764909
AGM	I	2016/02/11	2016/02/11	662733	2108792	XS0541620901	143876	2016/02/24	BHM			:								LU	2108792
AGM	I	2016/02/11	2016/02/11	662734	3933630	XS0856556807	143876	2016/02/24	BHM			:								LU	3933630
AGM	U	2016/02/01	2016/02/12	661499	1566622	XS0308666493	110953	2016/02/05	BHM			:	The Offices Of Reed Smith LLP,	The Broadgate Tower,	20 Primrose Street,	EC2A 2RS,	London			GB	1566622
AGM	U	2016/02/01	2016/02/12	661501	1566684	XS0308710143	110953	2016/02/05	BHM			:	The Offices Of Reed Smith LLP,	The Broadgate Tower,	20 Primrose Street,	EC2A 2RS,	London			GB	1566684
AGM	I	2016/02/12	2016/02/12	662868	624859	XS0308659795	110953	2016/02/05	BHM			:								GB	624859
AGM	U	2016/02/01	2016/02/15	661490	1566600	XS0308648673	110953	2016/02/05	BHM			:	The Offices Of Reed Smith LLP,	The Broadgate Tower,	20 Primrose Street,	EC2A 2RS,	London			GB	1566600
AGM	U	2016/02/11	2016/02/15	662730	4301305	IL0077101553	21009	2016/02/14	BHM			16:00								IL	4301305
AGM	U	2016/02/11	2016/02/15	662735	4301310	IL0077101637	21009	2016/02/14	BHM			16:00								IL	4301310
AGM	I	2016/02/13	2016/02/15	663194	3543081	NO0010635725	11526	2016/02/26	BHM			11:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt 1,	0161 Oslo - 6th Floor					NO	3543081
AGM	I	2016/02/13	2016/02/15	663195	3960231	NO0010669633	11526	2016/02/26	BHM			11:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt 1,	0161 Oslo - 6th Floor					NO	3960231
AGM	I	2016/02/13	2016/02/15	663196	4131259	NO0010691892	11526	2016/02/26	BHM			11:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt 1,	0161 Oslo - 6th Floor					NO	4131259
AGM	I	2016/02/13	2016/02/15	663197	4336875	NO0010717473	11526	2016/02/26	BHM			11:00	The Premises of Nordic Trustee ASA,	Haakon VIIs gt 1,	0161 Oslo - 6th Floor					NO	4336875
AGM	I	2016/02/15	2016/02/15	663204	4138810	IL0047301499	21554	2016/02/15	BHM			10:00								IL	4138810
AGM	I	2016/02/15	2016/02/15	663206	1980667	IL0047301234	136187	2016/02/15	BHM			10:00								IL	1980667
AGM	I	2016/02/15	2016/02/15	663213	3838472	IL0011213340	20943	2016/02/14	BHM			18:00	Co. Offices	TLV						IL	3838472
AGM	I	2016/02/15	2016/02/15	663217	1660593	BG2100005094	110489	2015/03/27	BHM			:								BG	1660593
AGM	U	2016/02/12	2016/02/15	662897	4220215	XS1046228950	79961	2016/03/01	BHM			:								ZA	4220215
AGM	U	2016/02/15	2016/02/16	663214	3838472	IL0011213340	20943	2016/02/15	BHM			08:00								IL	3838472
EDI_ENDOFFILE
